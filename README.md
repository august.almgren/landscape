# About

Landscape is a procedurally generated 3D world built using WebGL and three.js.

## Chunk Management

Worlds are divided into square chunks, which are loaded and unloaded dynamically whenever a player enters a new chunk based on the distance to it from the player's current chunk. Each chunk contains a set of 2D coordinates which are send to the GPU to be rendered. Rendering is divided into three steps: a vertex shader, a fragment shader, and a postprocessing step.

## Vertex Shader

The vertex shader appplies 3 layers of perlin noise with different amplitudes and periods (these were determined experimentally) to produce a height value for each 2D coordinate. In addition to the height, the perlin noise function also returns the partial `x` and `y` and `z` derivatives, which correspond to the normal at that location. The `x` and `y` normals are then divided by the `z` normal, setting them relative to `z = 1`, and passed to the fragment shader via a varying variable. The distance from the camera to the point is also computed and send to the fragment shader through a varying variable. If map mode is enabled, this is done in 2D. If not, it is done id 3D.

## Fragment Shader

Unlike standard fragment shaders, the main fragment shader does not return the color to be rendered onscreen. Rather, it stores the `x` and `y` components of the normal interpolated from the summed outputs of the the perlin noise functions computed in the pixel shader as the output `r` and `g` values. Since the normal components are set relative to `z = 1`, the `z` component does not need to be stored; instead the (interpolated) height is set as the `b` output. Finally, the interpolated distance from the camera to the point is used to compute a fade factor to hide the edges of the loaded map, which is sent as the `a` output component. Pixels belonging to the skybox are given a magic skybox value of 0.

To seed the perlin noise generation function, a permutation table is created and uploaded to the GPU when the program is first initialized. This makes it simple to create new maps, which is done by applying a Fisher Yates shuffle to permutation array and reuploading it to the GPU.

## Postprocessing Shader

Instead of being rendered to the screen, the outputs of the main fragment shader are instead rendered to a texture. A second fragment shader is then applied to each fragment in this texture. This pipes the height value of each texel through a function in order to determine the actual color of the texel - the lowest values become water, followed by sand, grass, rock and finally snow. If the height is set to the magic value reserved for the skybox, then the skybox is instead sampled to get the final color.

Once the terrain color has been determined, two sobel filters are then applied to add contours to the image, one based on the height and one baesd on the normal. The time is also sampled and passed through a sine function before being multiplied by the output color, resulting in a gradual day/night cycle.

# Controls

`W` move up

`A` turn left

`S` move down

`D` turn right

`M` toggle map mode

`R` load a new map

`L` toggle wireframe mode

`P` pause/unpause
