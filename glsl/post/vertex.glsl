varying vec2 texcoord;

void main() {

	// convert from position in range [-1,1] to texcoord in range [0,1]
	texcoord = position.xy / 2.0 + 0.5;

	gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}
