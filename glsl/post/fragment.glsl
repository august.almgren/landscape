varying vec2 texcoord;

uniform sampler2D main_map;
uniform sampler2D skybox_map;
uniform vec2 map_size;
uniform float ambient_light_intensity;

const float SOBEL_INTENSITY   = 4.0;
const float SOBEL_SENSITIVITY = 0.4;

// min and max elevations (horizon, baseline + sum of amplitudes)
const float MIN_Z =  0.2;
const float MAX_Z = 50.0;

// biome elevation boundaries
const float WATER_END   = (MIN_Z +  0.2) / MAX_Z;
const float SAND_START  = (MIN_Z +  0.5) / MAX_Z;
const float SAND_END    = (MIN_Z +  0.8) / MAX_Z;
const float GRASS_START = (MIN_Z +  1.8) / MAX_Z;
const float GRASS_END   = (MIN_Z + 11.8) / MAX_Z;
const float ROCK_START  = (MIN_Z + 12.8) / MAX_Z;
const float ROCK_END    = (MIN_Z + 16.3) / MAX_Z;
const float SNOW_START  = (MIN_Z + 16.8) / MAX_Z;

// biome colors
const vec3 WATER_COLOR = vec3(0.0, 0.0, 1.0);
const vec3 SAND_COLOR  = vec3(1.0, 1.0, 0.6);
const vec3 GRASS_COLOR = vec3(0.0, 0.8, 0.0);
const vec3 ROCK_COLOR  = vec3(0.4, 0.4, 0.4);
const vec3 SNOW_COLOR  = vec3(1.0, 1.0, 1.0);

float avg(vec3 v) {

	return (v.x + v.y + v.z) / 3.0;
}

float max(vec3 v) {

	float max = v.x;
	if (v.y > max) {max = v.y;}
	if (v.z > max) {max = v.z;}
	return max;
}

// map elevation to biome color
vec3 get_surface_color(float world_z) {

	if (world_z <= WATER_END) {
		return WATER_COLOR;
	} else if (world_z <= SAND_START) {
		float i = (world_z - WATER_END) / (SAND_START - WATER_END);
		return mix(WATER_COLOR, SAND_COLOR, i);
	} else if (world_z <= SAND_END) {
		return SAND_COLOR;
	} else if (world_z <= GRASS_START) {
		float i = (world_z - SAND_END) / (GRASS_START - SAND_END);
		return mix(SAND_COLOR, GRASS_COLOR, i);
	} else if (world_z <= GRASS_END) {
		return GRASS_COLOR;
	} else if (world_z <= ROCK_START) {
		float i = (world_z - GRASS_END) / (ROCK_START - GRASS_END);
		return mix(GRASS_COLOR, ROCK_COLOR, i);
	} else if (world_z <= ROCK_END) {
		return ROCK_COLOR;
	} else if (world_z <= SNOW_START) {
		float i = (world_z - ROCK_END) / (SNOW_START - ROCK_END);
		return mix(ROCK_COLOR, SNOW_COLOR, i);
	} else {
		return SNOW_COLOR;
	}
}

// magnitude functions used mainly for sobel filters

float norm_mag(float dx, float dy) {

	vec4 texel = texture2D(main_map, texcoord + vec2(dx, dy));
	vec3 norm = normalize(vec3(texel.xy, 1.0));
	return avg(norm);
}

float height_mag(float dx, float dy) {

	vec4 texel = texture2D(main_map, texcoord + vec2(dx, dy));
	return texel.z;
}

// sobel filters

vec3 sobel_normal_filter() {

	// get offsets
	float dx = 1.0 / map_size.x;
	float dy = 1.0 / map_size.y;

	// get texel grayscale magnitudes
	float g00 = norm_mag(-dx, -dy);
	float g01 = norm_mag(-dx, 0.0);
	float g02 = norm_mag(-dx,  dy);
	float g10 = norm_mag(0.0, -dy);

	float g12 = norm_mag(0.0,  dy);
	float g20 = norm_mag( dx, -dy);
	float g21 = norm_mag( dx, 0.0);
	float g22 = norm_mag( dx,  dy);

	float h = g00 - g02 + 2.0 * g10 - 2.0 * g12 + g20 - g22;
	float v = g00 + 2.0 * g01 + g02 - g20 - 2.0 * g21 - g22;
	//float m = SOBEL_INTENSITY * (abs(h) + abs(v)); // faster
	float m = SOBEL_INTENSITY * sqrt(h * h + v * v); // more accurate

	return 1.0 - vec3(m);
}

vec3 sobel_height_filter() {

	// get offsets
	float dx = 1.0 / map_size.x;
	float dy = 1.0 / map_size.y;

	// get texel grayscale magnitudes
	float g00 = height_mag(-dx, -dy);
	float g01 = height_mag(-dx, 0.0);
	float g02 = height_mag(-dx,  dy);
	float g10 = height_mag(0.0, -dy);

	float g12 = height_mag(0.0,  dy);
	float g20 = height_mag( dx, -dy);
	float g21 = height_mag( dx, 0.0);
	float g22 = height_mag( dx,  dy);

	float h = g00 - g02 + 2.0 * g10 - 2.0 * g12 + g20 - g22;
	float v = g00 + 2.0 * g01 + g02 - g20 - 2.0 * g21 - g22;
	//float m = SOBEL_INTENSITY * (abs(h) + abs(v)); // faster
	float m = SOBEL_INTENSITY * sqrt(h * h + v * v); // more accurate

	return 1.0 - vec3(m);
}

void main() {

	// xy = normal (normal.z = 1), z = height, w = alpha
	vec4 data = texture2D(main_map, texcoord);
	if (data.z != 0.0) {
		vec3 color = get_surface_color(data.z);
		color *= ambient_light_intensity;
		if (height_mag(0.0, 0.0) > WATER_END) {
			vec3 sobel =
				(color + sobel_normal_filter()) *
				(color + sobel_height_filter());
			// filter out non-black colors to avoid making scene lighter
			color *= (max(sobel) < SOBEL_SENSITIVITY) ? vec3(0.0) : vec3(1.0);
		}
		gl_FragColor = vec4(color, data.w);
	} else {
 		gl_FragColor = texture2D(skybox_map, texcoord);
	}
}
