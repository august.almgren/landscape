varying vec2 normal2d;
varying float world_z;
varying float distance_to_camera;

uniform int p[512]; // permutation hash table
uniform bool map_mode;

const float MIN_Z =  0.2;
const float MAX_Z = 50.0;

float modf(float a, float b) {

	return a - (b * floor(a / b));
}

int hash(float x, float y) {

	int xi = int(modf(x, 256.0));
	int yi = int(modf(y, 256.0));
	return p[xi + p[yi]];
}

float weight(int g, vec2 d) {

	int modg = int(modf(float(g), 4.0));
	return  ((modg == 0 || modg == 1) ? -d.x : d.x) +
		((modg == 0 || modg == 2) ? -d.y : d.y);
}

float fade(float i) {

	// use quintic fade function to smoothen interpolation
	// 6i^5 - 15i^4 + 10i^3
	return i * i * i * (i * (i * 6.0 - 15.0) + 10.0);
}

float norm(float i) {

	// the normal is computed using the derivative of the fade function
	// 30i^4 - 60i^3 + 30i^2
	return  i * i * (i * (i * 30.0 - 60.0) + 30.0);
}

vec4 perlin2D(vec2 world_position, float amplitude, float period) {

	float x = world_position.x / period;
	float y = world_position.y / period;

	// index hash table to get gradients
	int g00 = hash(x      , y      );
	int g01 = hash(x      , y + 1.0);
	int g10 = hash(x + 1.0, y      );
	int g11 = hash(x + 1.0, y + 1.0);

	// get edges and convert to range [0, 1]
	// this is also the distance from (0, 0)
	vec2 d00 = vec2(modf(x, 1.0), modf(y, 1.0));
	// get other distances from corners of unit square to normalized point
	vec2 d01 = d00 - vec2(0.0, 1.0);
	vec2 d10 = d00 - vec2(1.0, 0.0);
	vec2 d11 = d00 - vec2(1.0, 1.0);

	// get weights
	float w00 = weight(g00, d00);
	float w01 = weight(g01, d01);
	float w10 = weight(g10, d10);
	float w11 = weight(g11, d11);

	// apply fade function to get interpolation values
	float fx = fade(d00.x);
	float fy = fade(d00.y);

	// compute derivatives
	float dx = norm(d00.x);
	float dy = norm(d00.y);

	// interpolate the weights to get the height
	float height = mix(mix(w00, w10, fx), mix(w01, w11, fx), fy);

	// compute the normal using partial derivatives
	float nx = dx * ((w10 - w00) + fy * (w11 + w00 - w01 - w10));
	float ny = dy * ((w01 - w00) + fx * (w11 + w00 - w01 - w10));
	vec3 normal = normalize(vec3(-nx, -ny, 1.0));

	return vec4(normal, height) * amplitude;
}

void main() {

	vec4 world_position = modelMatrix * vec4(position, 1.0);

	vec4 pdata = vec4(0.0, 0.0, 0.0, 0.0); // base normal & elevation
	pdata += perlin2D(world_position.xy, 26.0, 411.0); // large pnoise
	pdata += perlin2D(world_position.xy, 16.0,  65.0); // mid pnoise
	pdata += perlin2D(world_position.xy,  8.0,  31.0); // small pnoise

	if (pdata.w <= MIN_Z) {
		// make water flat
		pdata = vec4(0.0, 0.0, 1.0, MIN_Z);
		normal2d = vec2(0.0);
		world_position.z = MIN_Z;
		world_z = MIN_Z / MAX_Z;
	} else {
		normal2d = pdata.xy / 3.0 / pdata.z; // average normals, set relative to z = 1
		world_position.z = pdata.w;
		world_z = pdata.w / MAX_Z;
	}

	vec4 clip_position = viewMatrix * world_position;
	distance_to_camera = map_mode
		? length(clip_position.xy)
		: length(clip_position);
	gl_Position = projectionMatrix * clip_position;
}
