varying vec2 normal2d;
varying float world_z;
varying float distance_to_camera;

uniform vec3 clear_color;

const float FADEOUT_START  = 300.0;
const float FADEOUT_END    = 325.0;
const float FADEOUT_LENGTH = FADEOUT_END - FADEOUT_START;

float get_fadeout_alpha() {

	// no need to clamp - done automatically when returning value
	return 1.0 - ((distance_to_camera - FADEOUT_START) / FADEOUT_LENGTH);
}

void main() {

	// return the unnormalized normal.xy, the height in world coordinates, and the alpha
	// these will be used to compute the final color in the post processing shader
	// the normal is relative to z=1, meaning it doesn't need to be sent
	gl_FragColor = vec4(normal2d, world_z, get_fadeout_alpha());
}
