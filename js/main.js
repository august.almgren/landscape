var renderer;

var main_scene;
var main_camera;
var main_uniforms;
var main_render_buffer;

var post_map_size;
var post_scene;
var post_camera;
var post_uniforms;

var start_countdown;
var clock;

var skybox_scene;
var skybox_render_buffer;
var skybox;

const CLEAR_COLOR = [0.0, 0.0, 0.0];

// used by perlin noise shader
// source: http://flafla2.github.io/2014/08/09/perlinnoise.html
let permutation_table = [151,160,137,91,90,15,
    131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,
    190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
    88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
    77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
    102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
    135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
    5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
    223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
    129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
    251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
    49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
    138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180
];

let wireframe_mode = false;
let paused = false;

// source: https://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array
function shuffle(a) {

    for (let i = a.length - 1; i > 0; i--) {
	    let j = Math.floor(Math.random() * (i + 1));
	    let x = a[i];
	    a[i] = a[j];
	    a[j] = x;
    }
	return a;
}

permutation_table = shuffle(permutation_table);
permutation_table = permutation_table.concat(permutation_table);

function toggle_wireframe_mode() {
	wireframe_mode = !wireframe_mode;
}

// key callbacks
document.addEventListener("keydown", function onDocumentKeyDown(event) {

	switch(event.which) {
	case 65: // a
		// start turning left
		left_turn_acceleration = TURN_ACCELERATION;
		breaking_left = false;
		break;
	case 68: // d
		// start turning right
		right_turn_acceleration = TURN_ACCELERATION;
		breaking_right = false;
		break;
	case 76: // l
		// toggle wireframe (line) rendering
		toggle_wireframe_mode();
		break;
	case 77: // m
		// toggle map mode
		toggle_map_mode(main_camera, main_uniforms);
		break;
	case 80: // p
		paused = !paused;
		break;
	case 82: // r
		// genereate a new map
		permutation_table = shuffle(permutation_table);
		break;
	case 83: // s
		// start moving down
		down_acceleration = VERT_ACCELERATION;
		breaking_down = false;
		break;
	case 87: // w
		// start moving up
		up_acceleration = VERT_ACCELERATION;
		breaking_up = false;
		break;
	}
}, false);

document.addEventListener("keyup", function onDocumentKeyUp(event) {

	switch(event.which) {
	case 65: // a
		// stop turning left
		left_turn_acceleration = 0.0;
		breaking_left = true;
		break;
	case 68: // d
		// stop turning right
		right_turn_acceleration = 0.0;
		breaking_right = true;
		break;
	case 83: // s
		// stop moving down
		down_acceleration = 0.0;
		breaking_down = true;
		break;
	case 87: // w
		// stop moving up
		up_acceleration = 0.0;
		breaking_up = true;
		break;
	}
}, false);

main();

function main() {

	// init stuff used by multiple shaders here
	main_render_buffer = new THREE.WebGLRenderTarget(window.innerWidth, window.innerHeight);
	skybox_render_buffer = new THREE.WebGLRenderTarget(window.innerWidth, window.innerHeight);

	// Load the shaders. They all call try_start() at the end
	// which continues once all have been loaded (start_countdown === 0)
	start_countdown = 2;
	load_shader('glsl/main/vertex.glsl', 'glsl/main/fragment.glsl', loaded_main_shader);
	load_shader('glsl/post/vertex.glsl', 'glsl/post/fragment.glsl', loaded_post_shader);
}

function loaded_main_shader(vertexShader, fragmentShader) {

	// setup main scene which generates terrain
	main_scene = new THREE.Scene();

	main_uniforms = {
		p: {type: "iv1", value: permutation_table},
		map_mode: {type: "b", value: get_map_mode()}
	};

	const main_material = new THREE.ShaderMaterial({
		uniforms: main_uniforms,
		vertexShader: vertexShader,
		fragmentShader: fragmentShader,
		transparent: true,
		blending: THREE.NoBlending // comment this out for trippyness
	});

	main_camera = init_main_camera(main_uniforms);

	init_chunks(main_camera.position, main_scene, main_material, wireframe_mode);

	// setup skybox scene (rendered separately from terrain)
	// TODO this could be done earlier (ex in <head>) to reduce initial delay before skybox appears
	skybox = init_skybox();
	skybox_scene = new THREE.Scene();
	skybox_scene.add(skybox);

	try_start();
}

function loaded_post_shader(vertexShader, fragmentShader) {

	// setup post scene for applying post processing effects
	post_scene = new THREE.Scene();

	var ambientLight = new THREE.AmbientLight(0xffffff);
	post_scene.add(ambientLight);

	var geometry = new THREE.PlaneGeometry(2, 2);
	post_uniforms = {
		main_map: {type: "t", value: main_render_buffer.texture},
		skybox_map: {type: "t", value: skybox_render_buffer.texture},
		map_size: {type: "v2", value: new THREE.Vector2(window.innerWidth, window.innerHeight)},
		ambient_light_intensity: {type: "f", value: 1.0}
	};

	var material = new THREE.ShaderMaterial({
		uniforms: post_uniforms,
		vertexShader: vertexShader,
		fragmentShader: fragmentShader,
		transparent: true

	});

	var bg = new THREE.Mesh(geometry, material);
	post_scene.add(bg);

	post_camera = new THREE.Camera();

	try_start();
}

// start game loop once all assets have been loaded
function try_start() {

	start_countdown -= 1;
	if (start_countdown === 0) {

		renderer = new THREE.WebGLRenderer({alpha: true});
		renderer.setSize(window.innerWidth, window.innerHeight);
		renderer.setClearColor(new THREE.Color(
				CLEAR_COLOR[0], CLEAR_COLOR[1], CLEAR_COLOR[2]));
		document.body.appendChild(renderer.domElement);

		// window resize callback
		window.onresize = function(event) {

			renderer.setSize(window.innerWidth, window.innerHeight);

			main_camera.aspect = window.innerWidth / window.innerHeight;
			main_camera.updateProjectionMatrix();

			post_camera.aspect = window.innerWidth / window.innerHeight;
			post_uniforms.map_size.value = new THREE.Vector2(
					window.innerWidth, window.innerHeight);
		};

		clock = new THREE.Clock();
		clock.start();
		requestAnimationFrame(tick);
	}
}

function tick() {

	update();
	render();
	requestAnimationFrame(tick);
}

function update() {

	update_main_camera(main_camera, paused);
	skybox.position.copy(main_camera.position);
	update_chunks(main_scene, main_camera.position, wireframe_mode);

	// update light (simulate day-night cycle)
	post_uniforms.ambient_light_intensity.value =
		Math.cos(clock.getElapsedTime() / 8.0) * 0.3 + 0.7;

}

function render() {

	//renderer.render(main_scene, main_camera); // debug render main shader to framebuffer
	renderer.render(main_scene, main_camera, main_render_buffer);
	renderer.render(skybox_scene, main_camera, skybox_render_buffer);
	renderer.render(post_scene, post_camera);
}
