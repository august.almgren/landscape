var left_turn_acceleration;
var right_turn_acceleration;
var up_acceleration;
var down_acceleration;
var left_turn_velocity;
var right_turn_velocity;
var up_velocity;
var down_velocity;
var breaking_left;
var breaking_right;
var breaking_up;
var breaking_down;
var yaw;

var map_mode;

const TURN_BREAK_FACTOR = 0.95;
const TURN_BREAK_THRESHOLD = 0.01;
const TURN_ACCELERATION = 0.4;
const MAX_YAW_VELOCITY = 1.25;

const VERT_BREAK_FACTOR = 0.80;
const VERT_BREAK_THRESHOLD = 0.01;
const VERT_ACCELERATION = 1.0;
const MAX_VERT_VELOCITY = 1.25;

function init_main_camera() {

	left_turn_acceleration = 0.0;
	right_turn_acceleration = 0.0;
	up_acceleration = 0.0;
	down_acceleration = 0.0;

	left_turn_velocity = 0.0;
	right_turn_velocity = 0.0;
	up_velocity = 0.0;
	down_velocity = 0.0;

	breaking_left = false;
	breaking_right = false;
	breaking_up = false;
	breaking_down = false;

	yaw = 0.0;

	main_camera = new THREE.PerspectiveCamera(60,
			window.innerWidth / window.innerHeight, 0.1, 1000);
	main_camera.up = new THREE.Vector3(0, 0, 1);

	set_map_mode(main_camera, main_uniforms, false);

	return main_camera;
}

function update_main_camera(main_camera, paused) {

	if (left_turn_velocity > MAX_YAW_VELOCITY) {
		left_turn_velocity = MAX_YAW_VELOCITY;
		left_turn_acceleration = 0.0;
	}
	if (breaking_left) {
		left_turn_velocity *= TURN_BREAK_FACTOR;
		if (left_turn_velocity <= TURN_BREAK_THRESHOLD) {
			left_turn_velocity = 0.0;
			breaking_left = false;
		}
	} else {
		left_turn_velocity += left_turn_acceleration;
	}

	if (right_turn_velocity > MAX_YAW_VELOCITY) {
		right_turn_velocity = MAX_YAW_VELOCITY;
		right_turn_acceleration = 0.0;
	}
	if (breaking_right) {
		right_turn_velocity *= TURN_BREAK_FACTOR;
		if (right_turn_velocity <= TURN_BREAK_THRESHOLD) {
			right_turn_velocity = 0.0;
			breaking_right = false;
		}
	} else {
		right_turn_velocity += right_turn_acceleration;
	}

	yaw += left_turn_velocity - right_turn_velocity;

	if (up_velocity > MAX_VERT_VELOCITY) {
		up_velocity = MAX_VERT_VELOCITY;
		up_acceleration = 0.0;
	} if (breaking_up) {
		up_velocity *= VERT_BREAK_FACTOR;
		if (up_velocity <= VERT_BREAK_THRESHOLD) {
			up_velocity = 0.0;
			breaking_up = false;
		}
	} else {
		up_velocity += up_acceleration;
	}

	if (down_velocity > MAX_VERT_VELOCITY) {
		down_velocity = MAX_VERT_VELOCITY;
		down_acceleration = 0.0;
	} if (breaking_down) {
		down_velocity *= VERT_BREAK_FACTOR;
		if (down_velocity <= VERT_BREAK_THRESHOLD) {
			down_velocity = 0.0;
			breaking_down = false;
		}
	} else {
		down_velocity += down_acceleration;
	}

	var vert_velocity = up_velocity - down_velocity;

	if (map_mode) {
		main_camera.rotation.z = yaw * Math.PI / 180;
		if (!paused) {
			main_camera.position.x += 2.5 * Math.sin(-main_camera.rotation.z);
			main_camera.position.y += 2.5 * Math.cos(main_camera.rotation.z);
		}
		main_camera.position.z += vert_velocity;
	} else {
		main_camera.rotation.y = yaw * Math.PI / 180;
		if (!paused) {
			main_camera.position.x += 2.5 * Math.sin(-main_camera.rotation.y);
			main_camera.position.y += 2.5 * Math.cos(main_camera.rotation.y);
		}
		main_camera.position.z += vert_velocity;
	}
}

function get_map_mode() {

	return map_mode;
}

function set_map_mode(main_camera, main_uniforms, new_mode) {

	map_mode = new_mode;
	main_uniforms.map_mode.value = map_mode;

	if (map_mode) {
		main_camera.position.z = 800;
		main_camera.lookAt(
			main_camera.position.x,
			main_camera.position.y,
			main_camera.position.z - 1
		);
	} else {
		main_camera.position.z = 50;
		main_camera.lookAt(
			main_camera.position.x,
			main_camera.position.y + 1,
			main_camera.position.z
		);
	}
}

function toggle_map_mode(main_camera, main_uniforms) {

	set_map_mode(main_camera, main_uniforms, !map_mode);
}
