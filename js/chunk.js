var chunk_faces;
var current_chunk_coords;

var main_material;

const CHUNK_SIZE = 32;
const HALF_CHUNK_SIZE = CHUNK_SIZE / 2;
const CHUNK_RADIUS = 10;
const CHUNK_DIAMETER = CHUNK_RADIUS * 2 + 1;
const NUM_CHUNKS = Math.pow(CHUNK_DIAMETER, 2);

function init_chunks(main_camera_position, main_scene, _main_material, wf_mode) {

	main_material = _main_material;

	current_chunk_coords = [
		Math.round(main_camera_position.x / CHUNK_SIZE),
		Math.round(main_camera_position.y / CHUNK_SIZE)
	];

	chunk_faces = [];
	// generate mesh from point array
	for (let y = 0; y < CHUNK_SIZE; y++) {
		for (let x = 0; x < CHUNK_SIZE; x++) {
			// add 1 to CHUNK_SIZE since # points = # squares + 1
			const nw = (CHUNK_SIZE + 1) * y + x;
			const ne = (CHUNK_SIZE + 1) * y + (x + 1);
			const sw = (CHUNK_SIZE + 1) * (y + 1) + x;
			const se = (CHUNK_SIZE + 1) * (y + 1) + (x + 1);

			chunk_faces.push(
				new THREE.Face3(nw, ne, se),
				new THREE.Face3(se, sw, nw)
			);
		}
	}

	for (let x = -CHUNK_RADIUS; x <= CHUNK_RADIUS; x++) {
		const _x = x + current_chunk_coords[0];
		for (let y = -CHUNK_RADIUS; y <= CHUNK_RADIUS; y++) {
			const _y = y + current_chunk_coords[1];
			const geometry = generate_chunk(_x, _y, wf_mode);
			let chunk = new THREE.Mesh(geometry, main_material);
			chunk.name = "chunk_" + _x + "_" + _y;
			main_scene.add(chunk);
		}
	}
}

function generate_chunk(chunk_x, chunk_y, wf_mode) {

	const center = new THREE.Vector2(CHUNK_SIZE * chunk_x, CHUNK_SIZE * chunk_y);

	let geometry = new THREE.Geometry();

	// generate flat square array of points
	for (let y = -HALF_CHUNK_SIZE; y <= HALF_CHUNK_SIZE; y++) {
		for (let x = -HALF_CHUNK_SIZE; x <= HALF_CHUNK_SIZE; x++) {
			geometry.vertices.push(new THREE.Vector3(center.x + x, center.y + y, 0));
		}
	}

	geometry.faces = chunk_faces;

	return wf_mode
		? new THREE.WireframeGeometry(geometry)
		: geometry;
}

function rem_chunk(main_scene, px, py, cx, cy) {

	const dx = Math.abs(px - cx);
	const dy = Math.abs(py - cy);
	if (dx > CHUNK_RADIUS || dy > CHUNK_RADIUS) {
		// remove chunk
		main_scene.remove(main_scene.getObjectByName("chunk_" + cx + "_" + cy));
	}
}

function add_chunk(main_scene, px, py, cx, cy, wf_mode) {

	const dx = Math.abs(px - cx);
	const dy = Math.abs(py - cy);
	if (dx <= CHUNK_RADIUS && dy <= CHUNK_RADIUS
			&& !main_scene.getObjectByName("chunk_" + cx + "_" + cy)) {

		// add chunk
		const geometry = generate_chunk(cx, cy, wf_mode);
		const chunk = wf_mode
			? new THREE.LineSegments(geometry, main_material)
			: new THREE.Mesh(geometry, main_material);
		chunk.name = "chunk_" + cx + "_" + cy;
		main_scene.add(chunk);
	}
}

function update_chunks(main_scene, main_camera_position, wf_mode) {

	// update loaded chunks
	const old_px = current_chunk_coords[0];
	const old_py = current_chunk_coords[1];
	const new_px = Math.round(main_camera_position.x / CHUNK_SIZE);
	const new_py = Math.round(main_camera_position.y / CHUNK_SIZE);

	if (old_px != new_px || old_py != new_py) {
		// update edge chunks
		for (let x = -CHUNK_RADIUS; x <= CHUNK_RADIUS; x++) {
			rem_chunk(main_scene, new_px, new_py, old_px + x, old_py - CHUNK_RADIUS);
			rem_chunk(main_scene, new_px, new_py, old_px + x, old_py + CHUNK_RADIUS);
		}
		for (let y = -CHUNK_RADIUS + 1; y < CHUNK_RADIUS; y++) {
			rem_chunk(main_scene, new_px, new_py, old_px - CHUNK_RADIUS, old_py + y);
			rem_chunk(main_scene, new_px, new_py, old_px + CHUNK_RADIUS, old_py + y);
		}
		for (let x = -CHUNK_RADIUS; x <= CHUNK_RADIUS; x++) {
			add_chunk(main_scene, new_px, new_py,
				new_px + x, new_py - CHUNK_RADIUS, wf_mode);
			add_chunk(main_scene, new_px, new_py,
				new_px + x, new_py + CHUNK_RADIUS, wf_mode);
		}
		for (let y = -CHUNK_RADIUS + 1; y < CHUNK_RADIUS; y++) {
			add_chunk(main_scene, new_px,
				new_py, new_px - CHUNK_RADIUS, new_py + y, wf_mode);
			add_chunk(main_scene, new_px, new_py,
				new_px + CHUNK_RADIUS, new_py + y, wf_mode);
		}

		current_chunk_coords = [new_px, new_py];
	}
}
