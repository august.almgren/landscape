function init_skybox() {

	const skybox_texture = new THREE.CubeTextureLoader().load([
		"img/bg-east.png" , "img/bg-west.png",
		"img/bg-south.png", "img/bg-north.png",
		"img/bg-down.png" , "img/bg-up.png"
	]);

	const skybox_material = new THREE.MeshBasicMaterial({
		envMap: skybox_texture,
		side: THREE.BackSide
	});

	const skybox_geometry = new THREE.BoxGeometry(1024, 1024, 1024);

	return new THREE.Mesh(skybox_geometry, skybox_material);
}
